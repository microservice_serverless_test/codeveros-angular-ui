import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {PageNotFoundComponent} from './core/not-found.component';
import {HomeComponent} from './home/home.component';
import { AuthGuard } from 'auth';
import {WelcomeComponent} from './welcome/welcome.component';

const routes: Routes = [
  {
    path: '',
    component: HomeComponent,
    canActivate: [ AuthGuard ],
    canActivateChild: [ AuthGuard ],
    children: [
      {
        path: '',
        children: [
          {
            path: '',
            component: WelcomeComponent
          },
          {
            path: 'training',
            loadChildren: './lib-wrappers/catalog-wrapper.module#CatalogWrapperModule'
          },
          {
            path: 'users',
            loadChildren: './lib-wrappers/user-wrapper.module#UserWrapperModule'
          }
        ]
      }
    ]
  },
  { path: '**', component: PageNotFoundComponent }
];

@NgModule({
  exports: [ RouterModule ],
  imports: [ RouterModule.forRoot(routes) ]
})
export class AppRoutingModule {}
